# unitdef.txt file (External Definition)
# This is a PyAI version of UNITDEF.INI made by Gold Dragon, used by ScAIEdit III

# Initially modified to include all unit, upgrade, and tech entries.
# Further modified by Veeq7 & Pr0nogo for Cosmonarchy BW
# https://www.fraudsclub.com/cosmonarchy-bw/

# AI IDs
military disruption_web_unit = 105
military dark_swarm_unit = 202
military left_pit_door = 207
military right_pit_door = 208

# Unused IDs
building left_upper_door = 205
building right_upper_door = 206

# Special
building special_control_node = 192

# Factional NGS units
military cohort = 50
military luminary = 150
military aspirant = 194
military tarasque = 210

# Terran military units
military maverick = 0
military eidolon = 1
military vulture = 2
military goliath = 3
military phalanx = 5
military phalanx_siege_mode = 30
military wraith = 8
military seraph = 9
@spellcaster(seraph)
military trojan = 11
military minotaur = 12
military nuclear_missile = 14
military harakan = 32
military cleric = 34
@spellcaster(cleric)
military valkyrie = 58

# Terran NGS units
building talos = 20
#
military apostle = 10
military shaman = 15
military madcap = 18
military cuirass = 19
military salamander = 22
military gorgon = 23
military phobos = 24
military madrigal = 27
military centaur = 28
military pazuzu = 49
military penumbra = 53
military cyclops = 55
military heracles = 77
military wyvern = 91
military cyprian = 92
military azazel = 98
military magnetar = 118
military olympian = 137
military autocrat = 145
military striga = 148
military siren = 151
military paladin = 186
military savant = 187
military blackjack = 195
military claymore = 196
military anticthon = 211
military sundog = 253
military ancile = 255
military matador = 256
military pegasus = 261
military hippogriff = 262
military oberon = 263
military aion = 264
military wendigo = 275
military southpaw = 276
military ramesses = 281
military durendal = 282
military cataphract = 283
military dilettante = 285
military sevenstep = 286
military hypnagogue = 287
military armistice = 289
military pythean = 290
military ambassador = 292
military heir = 293
military horseman = 294
military thespian = 297
military silvertongue = 298
military emperor = 300
military jester = 301

# Terran buildings
building mason = 7
#
building command_center = 106
building comsat_station = 107
building missile_silo = 108
building supply_depot = 109
building reservoir = 110
building barracks = 111
building fulcrum = 113
building starport = 114
building commandment = 115
building daedala = 116
building covert_ops = 117
building machine_shop = 120
building atlas = 122
building watchdog = 124
building bunker = 125

# Terran NGS buildings
building medbay = 119
building munitions_bay = 121
building ion_cannon = 127
building guildhall = 147
building fountainhead = 163
building quarry = 174
building tinkerers_tower = 175
building particle_accelerator = 189
building biotic_base = 190
building iron_foundry = 198
building captaincy = 199
building scrapyard = 200
building nanite_assembly = 203
building sentinel = 209
building atelier = 213
building treasury = 215
building starpad = 254
building future_station = 257
building quantum_institute = 259
building aerie = 260
building mantle = 280
building apothecary = 284
building parthenon = 288
building iron_council = 295
building amphitheater = 296
building throneroom = 299

# Protoss military units
military lanifect = 60
military cabalist = 61
military mind_tyrant = 63
@spellcaster(mind_tyrant)
military zealot = 65
military dracadin = 66
military cantavis = 67
@spellcaster(cantavis)
military archon = 68
military envoy = 69
military panoptus = 70
military didact = 71
military solarion = 72
military steward = 73
military witness = 84
military accantor = 352

# Protoss NGS units
military manifold = 16
military architect = 17
military star_sovereign = 21
military gladius = 25
military magister = 26
military legionnaire = 74
military amaranth = 75
military hierophant = 76
military atreus = 78
military ecclesiast = 79
military aurora = 80
military clarion = 81
military empyrean = 82
military demiurge = 86
military pariah = 87
military simulacrum = 89
military charlatan = 90
military exemplar = 94
military idol = 99
military golem = 100
military augur = 102
military barghest = 104
military vagrant = 172
military anthelion = 179
military empress = 183
military herald = 265
military epigraph = 266
military servitor = 306
military positron = 307
military analogue = 308
military mainstay = 309
military maven = 310
military cloudform = 311
military vibrance = 312
military oscilliant = 313
military potentate = 314
military sycophant = 315
military optecton = 316
military patriarch = 317
military apostate = 318
military bulwark = 331
military monogram = 348
military vassal = 360

# Protoss buildings
building scribe = 64
#
building nexus = 154
building lattice = 155
building pylon = 156
building aquifer = 157
building gateway = 160
building warden = 162
building cenotaph = 164
building ancestral_archives = 165
building embassy = 166
building stargate = 167
building fleet_beacon = 169
building synthetic_synod = 171

# Protoss NGS buildings
building artisan = 182
#
building automaton_register = 158
building prostration_stage = 159
building rogue_gallery = 161
building argosy = 168
building astral_omen = 184
building crucible = 201
building ardent_authority = 220
building pennant = 221
building monument_of_sin = 223
building sanctum_of_sorrow = 225
building grand_library = 226
building matrix = 252
building strident_stratum = 278
building auxiliary_oasis = 279
building sequestered_icon = 304
building unitary_union = 305
building hall_of_heretics = 349

# Zerg military units
military zethrokor = 37
military hydralisk = 38
military ultrokor = 39
military rilirokor = 40
military mutalisk = 43
military geszithalor = 44
military matraleth = 45
@spellcaster(matraleth)
military isthrathaleth = 46
@spellcaster(isthrathaleth)
military vithrilisk = 62
military lakizilisk = 103

# Zerg NGS units
military othstoleth = 57
military alaszileth = 173
military iroleth = 222
#
military almaksalisk = 29
military alkajelisk = 48
military kalkalisk = 51
military evigrilisk = 52
military nathrokor = 54
military gorgrokor = 56
military rililisk = 93
military quazilisk = 95
military keskathalor = 96
military zoryusthaleth = 97
military liiralisk = 112
military sovroleth = 123
military kagralisk = 126
military vorvrokor = 128
military bactalisk = 129
military izirokor = 180
military skithrokor = 185
military axitrilisk = 191
military konvilisk = 217
military akistrokor = 218
military zarcavrokor = 258
military tethzorokor = 267
military protathalor = 268
military vilgorokor = 269
military cikralisk = 270
military tosgrilisk = 271
military ghitorokor = 272
military zobriolisk = 273
military tetcaerokor = 274
military selkerilisk = 277
military ogroszaleth = 291
military agrileth = 302
military mortothrokor = 319
military cherbrathalor = 320
military okrimnilisk = 321
military algoztalisk = 322
military elthrazalisk = 323
military begrothilisk = 324
military oztukrokor = 325
military kethrostalor = 326
military rakzilisk = 327

# Zerg misc
military larva = 35
military larva_egg = 36
military cocoon = 59
military ocsal_larva = 353

# Zerg buildings
building droleth = 41
building ovileth = 42
#
building hachirosk = 131
building caphrolosk = 132
building gathtelosk = 133
building nydus_cesiant = 134
building hydrok_den = 135
building elthisth_mound = 136
building matravil_nest = 138
building ultrok_cavern = 140
building muthrok_spire = 141
building quazrok_pool = 142
building cagrant_colony = 143
building spraith_colony = 144
building surkith_colony = 146
building excisant = 149

# Zerg NGS buildings
building gosvileth = 47
#
building orboth_omlia = 130
building almakis_antre = 139
building othstol_oviform = 152
building gorgral_swamp = 153
building izkag_iteth = 170
building irulent_ivoa = 181
building zorkiz_shroud = 193
building vithrath_haunt = 197
building liivral_pond = 204
building alkaj_chasm = 212
building larvosk_colony = 216
building geszkath_grotto = 219
building irol_iris = 224
building sibraelosk = 227
building rakzogth_roost = 303
building axtoth_axis = 328
building begrozith_barrow = 329
building chokralgant_chazsal = 330
building nephrilosk = 350

# Misc units
military goliath_turret = 4
military t_tank_turret = 6
military spider_mine = 13
military t_siege_turret = 31
military scanner_sweep = 33
military plasma_shell = 85
military map_revealer = 101

# Neutral buildings
building mineral_field1 = 176
building mineral_field2 = 177
building mineral_field3 = 178
building vespene_geyser = 188
building start_location = 214

# Terran upgrades
upgrade t_infantry_armor = 0
upgrade t_vehicle_plating = 1
upgrade t_ship_plating = 2
upgrade t_infantry_weapon = 7
upgrade t_vehicle_weapon = 8
upgrade t_ship_weapon = 9
upgrade maverick_range = 16
upgrade vulture_speed = 17
upgrade seraph_mana = 19
upgrade eidolon_sight = 20
upgrade eidolon_mana = 21
upgrade wraith_mana = 22
upgrade minotaur_mana = 23
upgrade cleric_mana = 51
upgrade goliath_range = 54

# Terran tech
technology stim_packs = 0
technology spider_mines = 3
technology siege_mode = 5
technology defensive_matrix = 6
technology irradiate = 7
technology yamato_gun = 8
technology cloaking_field = 9
technology personnel_cloaking = 10
technology medstims_unused = 24
technology optical_flare = 30

# Terran NGS tech
technology lobotomy_mines = 1
technology reverse_thrust = 2
technology safety_off = 26
technology lazarus_agent = 32
technology tempo_change = 36
technology skyfall = 37
technology nanite_field = 38
technology observance = 39
technology sublime_shepherd = 40
technology power_siphon = 41
technology energy_decay = 42

# Protoss upgrades
upgrade p_armor = 5
upgrade p_plating = 6
upgrade p_ground_weapon = 13
upgrade p_air_weapon = 14
upgrade p_shields = 15
upgrade dragoon_range = 33
upgrade zealot_speed = 34
upgrade reinforced_scarabs = 35
upgrade reaver_capacity = 36
upgrade shuttle_speed = 37
upgrade observer_sight = 38
upgrade observer_speed = 39
upgrade templar_mana = 40
upgrade scout_sight = 41
upgrade scout_speed = 42
upgrade carrier_capacity = 43
upgrade arbiter_mana = 44
upgrade lanifect_mana = 47
upgrade mind_tyrant_mana = 49

# Protoss tech
technology psionic_storm = 19
technology hallucination = 20
technology recall = 21
technology stasis_field = 22
technology disruption_web = 25
technology mind_control = 27
technology feedback = 29
technology maelstrom = 31

# Protoss NGS tech
technology grief_of_all_gods = 29
technology khaydarin_eclipse = 33

# Zerg upgrades
upgrade z_carapace = 3
upgrade z_flyer_carapace = 4
upgrade z_melee_attack = 10
upgrade z_missile_attack = 11
upgrade z_flyer_attack = 12
upgrade overlord_capacity = 24
upgrade overlord_sight = 25
upgrade overlord_speed = 26
upgrade zethrokor_speed = 27
upgrade zethrokor_attack = 28
upgrade hydralisk_speed = 29
upgrade hydralisk_range = 30
upgrade queen_mana = 31
upgrade defiler_mana = 32
upgrade ultrokor_armor = 52
upgrade ultrokor_speed = 53

# Zerg tech
technology burrowing = 11
technology infestation = 12
technology ensnaring_brood = 13
technology dark_swarm = 14
technology plague = 15
technology consume = 16
technology parasite = 18

# Zerg NGS tech
technology reconstitution = 26
technology swarming_omen = 42

# Misc upgrades
upgrade mining_efficiency = 58
upgrade unused_60 = 60

# Misc tech
technology unused_43 = 43